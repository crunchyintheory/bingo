var searchParams;

function utoa(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

function atou(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

function random(seed) {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}

// http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffleArray(array, seed) {
    var currentIndex = array.length
      , temporaryValue
      , randomIndex
      ;
    
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
    
        // Pick a remaining element...
        randomIndex = Math.floor(random(seed) * currentIndex);
        currentIndex -= 1;
        
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
  
    return array;
}

function newSeed() {
    return Math.floor(Math.random() * 500);
}

function gen(shuffle, forceReshuffle = false) {
    let text = atou(searchParams.get('q')).split('\n').slice(0, 25);
    let array = text;
    let seed = (shuffle && searchParams.has('shuffle') && !forceReshuffle) ? searchParams.get('shuffle') : newSeed();
    console.log(seed);
    console.log(array);
    if(shuffle && array.length > 1) array = shuffleArray(text, seed / 500);
    console.log(array);
    let i = 0;
    if(searchParams.has('free')) {
        $('tbody td:not(.free) span').each(function() {
            $(this).text(array[i++]);
        });
        $('tbody td.free span').text(atou(searchParams.get('free')));
    }
    else {
        $('tbody td.free strong').addClass('d-none');
        $('tbody td span').each(function() {
            $(this).text(array[i++]);
        });
    }

    let url = new URL(window.location);
    if(shuffle) url.searchParams.set('shuffle', seed);
    url.searchParams.delete('noRedirect');
    $('#url input').val(url);
    $('tbody td').removeClass("done");
}

jQuery(function($) {
    searchParams = new URLSearchParams(window.location.search);
    if(searchParams.has('q')) {
        let shuffle = true;
        if(searchParams.has('shuffle')) {
            shuffle = searchParams.get('shuffle') != 'false';
        }
        gen(shuffle);
        
        let editor = new URL('editor.html', window.location.href);
        editor.search = window.location.search;
        editor.searchParams.delete('noRedirect');
        $('a[href="editor.html"]').attr('href', editor.toString());

    } else {
        if(!searchParams.has('noRedirect')) {
            window.location.href += 'editor.html';
        }
        $('.row.only-if-valid').remove();
    }
    $('tbody td').click(function() {
        $(this).toggleClass('done');
    });  
});

function toggleURL() { // eslint-disable-line no-unused-vars
    $('#url').toggleClass('hidden');
}

function copyURL() { // eslint-disable-line no-unused-vars
    $('#url input').select();

    document.execCommand('copy');
}
